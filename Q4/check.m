function [] = check(I, h1, s1, v1, x, y, x1, y1, TH)
    global I2
    m = size(I,1);
    n = size(I,2);
    a1 = I(x,y,1);
    b1 = I(x,y,2);
    c1 = I(x,y,3);
    a2 = I(x1,y1,1);
    b2 = I(x1,y1,2);
    c2 = I(x1,y1,3);
    if ((a1-a2) > TH ||(b1-b2) > TH ||(c1-c2) > TH )
        return;
    end
    I2(x1,y1) = 1;
    newimage(x1,y1,1) = h1;
    newimage(x1,y1,2) = s1;
    newimage(x1,y1,3) = v1;
    if (x1 + 1  < m && I2(x1+1, y1) == 0)
        check(I,h1, s1, v1, x1, y1,x1+1,y1,TH);
    end
    if (x1 - 1  > 0 && I2(x1- 1, y1) == 0)
        check(I,h1, s1, v1, x1, y1,x1-1,y1,TH);
    end
    if (y1 + 1  < n && I2(x1, y1+1) == 0)
        check(I,h1, s1, v1, x1, y1,x1,y1+1,TH);
    end
    if (y1 - 1  > 0 && I2(x1, y1-1) == 0)
        check(I,h1, s1, v1, x1, y1,x1,y1-1,TH);
    end
end

