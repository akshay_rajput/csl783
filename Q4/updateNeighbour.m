function [] = updateNeighbour(x, y, h, s, v, TH)
    global I;global I2;global newimage;global queue;
    m = size(I,1);
    n = size(I,2);
    if x+1 < m && I2(x+1, y) == 0 && (abs(I(x+1,y,1)-I(x,y,1))<=TH) && (abs(I(x+1,y,2)-I(x,y,2))<=TH) && (abs(I(x+1,y,3)-I(x,y,3))<=TH)
        I2(x+1, y) = 1;
        queue = [queue; x+1 y];
        newimage(x+1,y,1) = h;
        newimage(x+1,y,2) = s;
        newimage(x+1,y,3) = v;
    end
    
    if x-1 > 0 && I2(x-1, y) == 0 && (abs(I(x-1,y,1)-I(x,y,1))<=TH) && (abs(I(x-1,y,2)-I(x,y,2))<=TH) && (abs(I(x-1,y,3)-I(x,y,3))<=TH)
        I2(x-1, y) = 1;
        queue = [queue; x-1 y];
        newimage(x-1,y,1) = h;
        newimage(x-1,y,2) = s;
        newimage(x-1,y,3) = v;
    end
    
    if y+1 < n && I2(x, y+1) == 0 && (abs(I(x,y+1,1)-I(x,y,1))<=TH) && (abs(I(x,y+1,2)-I(x,y,2))<=TH) && (abs(I(x,y+1,3)-I(x,y,3))<=TH)
        I2(x, y+1) = 1;
        queue = [queue; x y+1];
        newimage(x,y+1,1) = h;
        newimage(x,y+1,2) = s;
        newimage(x,y+1,3) = v;
    end
    
    if y-1 > 0 && I2(x, y-1) == 0 && abs(I(x,y-1,1)-I(x,y,1))<=TH && abs(I(x,y-1,2)-I(x,y,2))<=TH && abs(I(x,y-1,3)-I(x,y,3))<=TH
        I2(x, y-1) = 1;
        queue = [queue; x y-1];
        newimage(x,y-1,1) = h;
        newimage(x,y-1,2) = s;
        newimage(x,y-1,3) = v;
    end
end