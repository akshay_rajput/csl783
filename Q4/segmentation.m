clc;clear;
global I;
I = imread('u.jpg');
figure, imshow(I);
I = rgb2hsv(I);
figure, imshow(I);
I = double(I);
m = size(I,1);
n = size(I,2);
global I2; global newimage; global queue;
I2 = zeros(m,n);
newimage = zeros(m,n,3);
queue = [];
TH = 0.05;
for i=1:m
    for j=1:n
        if I2(i,j) ~= 0
            continue;
        end
        queue = [queue;i j];
        I2(i,j) = 1;
        pixel_h = I(i,j,1);
        pixel_s = I(i,j,2);
        pixel_v = I(i,j,3);
        newimage(i,j,1) = pixel_h;
        newimage(i,j,2) = pixel_s;
        newimage(i,j,3) = pixel_v;
        while(size(queue,1)>0)
            x = queue(1,1);
            y = queue(1,2);
            updateNeighbour(x, y, pixel_h, pixel_s, pixel_v, TH);
            I2(x,y) = 2;
            queue = queue(2:end,:);
        end
    end
end
newnewimage = hsv2rgb(newimage);

figure,imshow(newnewimage);