function [Y] = medianCut(I, level, color)
    I=double(I);
    his=zeros(1,256);
    for i=1:size(I,1)
        for j=1:size(I,2)
            k=I(i,j,color);
            his(k+1)=his(k+1)+1;
        end
    end
    y=1;
   
    for c=1:256

        if his(c)==0
            continue
        end

        for x=1:his(c)
            img(y)=c-1;
            y=y+1;
        end
    end
    n=size(img,2);
    a=mod(n,level);
    s=(n-a)/level;
    m=1;
    if a~=0
        for i=1:a
            for j=1:s+1
                sub(j)=img(m+j-1);
            end
            m=m+s+1;
            maximum=max(sub);
            minimum=min(sub);
            ort=round(sum(sub)/(s+1));

            for z=1:size(I,1)
                for t=1:size(I,2)
                    if (I(z,t,color)<= maximum) && (I(z,t,color)>=minimum)
                        I(z,t,color)=ort;
                    end
                end
            end

        end
    end
    for i=a+1:level
    for j=1:s
        subb(j)=img(m+j-1);
    end
    m=m+s;
    maximum=max(subb);
    minimum=min(subb);
    ort=round(sum(subb)/(s));
     
    for z=1:size(I,1)
        for t=1:size(I,2)
            if (I(z,t,color)<= maximum) && (I(z,t,color)>=minimum)
                I(z,t,color)=ort;
            end
        end
    end
     
end
   
Y=uint8(I);
end

