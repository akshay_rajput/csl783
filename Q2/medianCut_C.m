% This is for color image quantization
clc;clear;clear all;
img = imread('lena.jpg');
imshow(img);
level = 8;
%img = double(img);
%minr = 256;
%maxr = -1;
% ming = 256;
% maxg = -1;
% minb = 256;
% maxb = -1;
% m = size(img,1);
% n = size(img,2);
% for i=1:m
%     for j=1:n
%         if img(i,j,1) > maxr
%             maxr = img(i,j,1);
%         end
%         if img(i,j,2) > maxg
%             maxg = img(i,j,2);
%         end
%         if img(i,j,3) > maxb
%             maxb = img(i,j,3);
%         end
%         if img(i,j,1) < minr
%             minr = img(i,j,1);
%         end
%         if img(i,j,2) < ming
%             ming = img(i,j,2);
%         end
%         if img(i,j,3) < maxb
%             minb = img(i,j,3);
%         end
%     end
% end
% range_r = maxr-minr;
% range_g = maxg-ming;
% range_b = maxb-minb;
% if range_r >= range_b && range_r >= range_g
%     color = 1;
% else if range_g >= range_b && range_g >= range_r
%         color = 2;
%     end
%      if range_b >= range_r && range_b >= range_g
%         color = 3;
%     end
% end

Y = medianCut(img,level,1);
Y = medianCut(Y,level,2);
Y = medianCut(Y,level,3);
figure, imshow(Y);