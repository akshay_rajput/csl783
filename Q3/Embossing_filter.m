%embossing filter
% -1 -1 0
% -1  0 1
%  0  1 1
clc;clear;
I = imread('lena.jpg');
I = double(I);
m = size(I,1);
n = size(I,2);
newimage=zeros(m,n,3);
for i=1:m
    for j=1:n
        if (i-1) > 0
            newimage(i,j, 1) = newimage(i,j, 1)-I(i-1,j,1);
            newimage(i,j, 2) = newimage(i,j, 2)-I(i-1,j,2);
            newimage(i,j, 3) = newimage(i,j, 3)-I(i-1,j,3);
            if (j-1) > 0
                newimage(i,j, 1) = newimage(i,j, 1)-I(i-1,j-1,1);
                newimage(i,j, 2) = newimage(i,j, 2)-I(i-1,j-1,2);
                newimage(i,j, 3) = newimage(i,j, 3)-I(i-1,j-1,3);
            end
        end
        
        if (i+1) < m
            newimage(i,j, 1) = newimage(i,j, 1)+I(i+1,j,1);
            newimage(i,j, 2) = newimage(i,j, 2)+I(i+1,j,2);
            newimage(i,j, 3) = newimage(i,j, 3)+I(i+1,j,3);
            if (j+1) < n
                newimage(i,j, 1) = newimage(i,j, 1)+I(i+1,j+1,1);
                newimage(i,j, 2) = newimage(i,j, 2)+I(i+1,j+1,2);
                newimage(i,j, 3) = newimage(i,j, 3)+I(i+1,j+1,3);
            end
        end
        if (j-1) > 0
            newimage(i,j, 1) = newimage(i,j, 1)-I(i,j-1,1);
            newimage(i,j, 2) = newimage(i,j, 2)-I(i,j-1,2);
            newimage(i,j, 3) = newimage(i,j, 3)-I(i,j-1,3);
        end
        
        if (j+1) < n
            newimage(i,j, 1) = newimage(i,j, 1)+I(i,j+1,1);
            newimage(i,j, 2) = newimage(i,j, 2)+I(i,j+1,2);
            newimage(i,j, 3) = newimage(i,j, 3)+I(i,j+1,3);
        end
        newimage(i,j,1) = newimage(i,j,1)+128;
        newimage(i,j,2) = newimage(i,j,2)+128;
        newimage(i,j,3) = newimage(i,j,3)+128;
    end
end
newimage = uint8(newimage);
figure, imshow(newimage);