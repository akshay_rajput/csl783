function [result] = gauss( x,y,sigma )
    t = x*x+y*y;
    d = 2*sigma*sigma;
    result = exp(-t/d)/(sigma*sqrt(2*pi));
end

