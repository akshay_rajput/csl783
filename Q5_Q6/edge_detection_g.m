clc;clear;
I = imread('3.jpg');
figure,imshow(I);
I = double(rgb2gray(I));
mask31=zeros(3,3);
mask32=zeros(3,3);

for i = 1:3
    for j= 1:3
        mask31(i,j) = gauss(i-2,j-2,1);
        mask32(i,j) = gauss(i-2,j-2,3);
    end
end
sum1 = sum(sum(mask31));
sum2 = sum(sum(mask32));
[m,n] = size(I(:,:,1));
image1 = zeros(m,n);
image2 = zeros(m,n);
for i = 1:m
    for j = 1:n
            image1(i,j) = image1(i,j)+I(i,j)*mask31(2,2);
            image2(i,j) = image2(i,j)+I(i,j)*mask32(2,2);
            if j-1 > 0
                image1(i,j) = image1(i,j)+I(i,j-1)*mask31(2,1);
                image2(i,j) = image2(i,j)+I(i,j-1)*mask32(2,1);
            end
            if j+1 < n
                image1(i,j) = image1(i,j)+I(i,j+1)*mask31(2,3);
                image2(i,j) = image2(i,j)+I(i,j+1)*mask32(2,3);
            end
            if i-1 > 0
                image1(i,j) = image1(i,j)+I(i-1,j)*mask31(1,2);
                image2(i,j) = image2(i,j)+I(i-1,j)*mask32(1,2);
                
                if j-1>0
                    image1(i,j) = image1(i,j)+I(i-1,j-1)*mask31(1,1);
                    image2(i,j) = image2(i,j)+I(i-1,j-1)*mask32(1,1);
                end
                if j+1<n
                    image1(i,j) = image1(i,j)+I(i-1,j+1)*mask31(1,3);
                    image2(i,j) = image2(i,j)+I(i-1,j+1)*mask32(1,3);
                end
            end
            
            if i+1<m
                image1(i,j) = image1(i,j)+I(i+1,j)*mask31(3,2);
                image2(i,j) = image2(i,j)+I(i+1,j)*mask32(3,2);
                if j-1>0
                    image1(i,j) = image1(i,j)+I(i+1,j-1)*mask31(3,1);
                    image2(i,j) = image2(i,j)+I(i+1,j-1)*mask32(3,1);
                end
                if j+1 < n
                    image1(i,j) = image1(i,j)+I(i+1,j+1)*mask31(3,3);
                    image2(i,j) = image2(i,j)+I(i+1,j+1)*mask32(3,3);
                end
            end
            image1(i,j) = image1(i,j)/sum1;
            image2(i,j) = image2(i,j)/sum2;
    end
end
figure,imshow(uint8(image1));
figure,imshow(uint8(image2));
newimage = double(uint8(image1)-uint8(image2));
min1 = min(newimage(:));
max1 = max(newimage(:));

newimage = ((newimage-min1)*255/(max1-min1));
% newimage(:,:,2) = ((newimage(:,:,2)-min1)*255/(max1-min1));
% newimage(:,:,3) = ((newimage(:,:,3)-min1)*255/(max1-min1));
figure,imshow(uint8(newimage));